#!/bin/bash

# Copyright (c) 2017 Universita' degli Studi di Napoli Federico II
# Author: Pasquale Imputato <p.imputato@gmail.com>

# This script aims to simplify the host configuration to operate in emulation
# mode in ns-3. ns-3 support emulation mode through standard file descriptor and through
# netmap file descriptor.

# The user can provide the following input parameters:
# - BEGINEND configuration time in {begin, end} to start/restore the host configuration
# - INAME interface name to be configured
# - EMMODE emulation mode in {raw-socket, netmap-generic, netmap-generic-pfifo, netmap-native}
# - NETMAPKO netmap.ko module path to be loaded (if one of the netmap emulation modes is chosed)

# For example, the user can run:
# sh netmap-config.sh begin enp2s0f0 netmap-generic-pfifo /home/netmap.ko

# Then it restores the configuration with
# sh netmap-config.sh end enp2s0f0 netmap-generic-pfifo

# The script does not check either the input parameters from the user
# or the system output.

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Usage:
# ./netmap-config.sh <begin/end> <emulation-mode> <iname>
# Whether to set up ('begin') or teardown ('end')
BEGINEND=$1
# Interface name; e.g. 'enp6s0f2'
INAME=$2
# Emulation mode:  one of 'raw-socket', 'netmap-generic', 'netmap-generic-pfifo', or 'netmap-native'
EMMODE=$3
# Point this to the location of your netmap kernel module; e.g.
#NETMAPKO=/lib/modules/5.4.0-47-generic/extra/netmap.ko
NETMAPKO=$4

date

if [ $BEGINEND = 'begin' ]
then

sudo ip link set $INAME promisc on

if [ $EMMODE = 'raw-socket' ]
then
  echo 'host configuration for raw-socket emulation done'
  # PACKET_QDISC_BYPASS to bypass the host traffic-control is configurable through ns-3 EmuFdNetDeviceHelper 
fi

if [ $EMMODE = 'netmap-generic' ] || [ $EMMODE = 'netmap-generic-pfifo' ] || [ $EMMODE = 'netmap-native' ]
then
  # load netmap module
  sudo insmod $NETMAPKO

  sudo ethtool -K $INAME tx off rx off
  sudo ethtool -K $INAME gso off gro off
fi

if [ $EMMODE = 'netmap-generic' ]
then
  sudo sh -c 'echo '2' > /sys/module/netmap/parameters/admode'
  sudo sh -c 'echo '1' > /sys/module/netmap/parameters/generic_txqdisc'
  echo 'host configuration for netmap-generic emulation done'

elif [ $EMMODE = 'netmap-generic-pfifo' ]
then
  sudo sh -c 'echo '2' > /sys/module/netmap/parameters/admode'
  sudo sh -c 'echo '0' > /sys/module/netmap/parameters/generic_txqdisc'
  sudo tc qdisc replace dev $INAME root pfifo_fast
  echo 'host configuration for netmap-generic-pfifo emulation done'

elif [ $EMMODE = 'netmap-native' ]
then
  # you should load before the driver provided by netmap for your interface
  # the switching operation can fail due to forced native mode
  sudo sh -c 'echo '1' > /sys/module/netmap/parameters/admode'
  echo 'host configuration for netmap-native emulation done'
fi

  echo 'begin configuration done'



elif [ $BEGINEND='end' ]
then

  sudo ip link set $INAME promisc off

  if [ $EMMODE = 'netmap-generic' ] || [ $EMMODE = 'netmap-generic-pfifo' ] || [ $EMMODE = 'netmap-native' ]
  then
    # unload netmap module
    sudo rmmod netmap

    sudo ethtool -K $INAME tx on rx on
    sudo ethtool -K $INAME gso on gro on
  fi
    echo 'end configuration done'

fi


