#!/bin/bash
export RTE_SDK=/usr/local/src/dpdk-stable-19.11.4
export RTE_TARGET=x86_64-native-linuxapp-gcc

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Load DPDK drivers
modprobe uio_pci_generic
modprobe uio
modprobe vfio-pci

# Configure the next step based on your environment
#modprobe igb_uio # for ubuntu package
# OR
insmod $RTE_SDK/$RTE_TARGET/kmod/igb_uio.ko # for dpdk source

# Configure hugepages
echo 256 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages


