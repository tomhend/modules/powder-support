# POWDER wireless testbed support module

This ns-3 extension module provides ns-3 code, scripts, and documentation
to help with the alignment of [ns-3](https://www.nsnam.org) with the
[POWDER-RENEW wireless testbed](https://powderwireless.net).

## Background

POWDER-RENEW is a regional advanced wireless testbed being deployed around
the University of Utah campus, Salt Lake City.  POWDER provides remote
access to a system that can configure testbed resources (including SDRs
and general compute nodes).

While ns-3 can be downloaded and installed on POWDER nodes without any
special modification, this repository is to collect documentation, scripts,
and ns-3 extensions that may make it easier for users to use ns-3 with POWDER.

## Contents

This repository is organized as an ns-3 extension module.

* Documentation about using ns-3 with POWDER is found in the `doc/` directory.
* Scripts to help with configuring POWDER for ns-3 emulation are found in the scripts/ directory
* Code for ns-3 models related to POWDER are found in `model/`, `test/`, and `examples/` directories

## Acknowledgment of support

This work was funded in part by NSF award number 1836725:  ICE-T: RC: Performance Evaluation of Advanced Wireless Network Edge Infrastructure - Network Simulation \& Test Beds
